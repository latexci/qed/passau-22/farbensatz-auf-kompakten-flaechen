# Farbensatz auf kompakten Flächen 

These are the lecture notes for the 'Farbensatz auf kompakten Flächen', taught in NONE at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://latexci.gitlab.io/qed/passau-22/farbensatz-auf-kompakten-flaechen/Farbensatz_kompakte_Flaechen.pdf
[2]: https://latexci.gitlab.io/qed/passau-22/farbensatz-auf-kompakten-flaechen/Farbensatz_kompakte_Flaechen.log
[3]: https://latexci.gitlab.io/qed/passau-22/farbensatz-auf-kompakten-flaechen/
