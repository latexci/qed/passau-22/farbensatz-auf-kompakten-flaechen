set -e
echo "Building document"
make pdf
mkdir public
mv build/Farbensatz_kompakte_Flaechen.pdf public
mv build/Farbensatz_kompakte_Flaechen.log public
cd public/
if ! command -v tree &> /dev/null
then
  echo "No tree utility found, skipping making tree"
else
  tree -H '.' -I "index.html" -D --charset utf-8 -T "Farbensatz auf kompakten Flächen" > index.html
fi
